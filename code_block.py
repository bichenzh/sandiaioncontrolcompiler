params_vars = '''#Constants
#const DDSCooling = 0
const DDSRaman_CounterProp = 6
const DDSRaman_CoProp = 7
const DDSRaman_CounterProp_SBCool = 6
const DDSRaman_CounterCarrier = 7
const DDSRamanSB1 = 4
const DDSRamanSB2 = 5
const DDSRaman_CounterProp_Parity = 2
const DDSRaman_Indiv1 = 3
const DDSRaman_Indiv2 = 1
const DDSRaman_Indiv2MS = 0
const DDSMW = 7
const PMTChannel = 3
const debugchannel = 5
const debugchannel2 = 6
const ShuttleCommand = 3
const memsCommand = 20 # =0x14, 1: address, 4: command

# masks and shutters
shutter InitializationShutter = 1
masked_shutter CoolingOn
masked_shutter CoolingOnRamanOnLockOff
masked_shutter CoolingOff_RamanOffLockOff
masked_shutter PumpingOn
masked_shutter MicrowaveOn
masked_shutter DetectOn
masked_shutter RamanOn_CoProp
masked_shutter RamanOn_CoProp2
masked_shutter RamanOn_CounterProp
masked_shutter RamanOn_CounterCarrier
masked_shutter RamanOn_CounterProp_2
masked_shutter RamanOn_CounterProp_Parity

masked_shutter RamanSBOn
masked_shutter RamanMSOn
masked_shutter RamanAMMSOn
masked_shutter DelayOn
masked_shutter DelayOn2
masked_shutter RamanOn_IndivFirst
masked_shutter RamanOn_IndivFirstMS
masked_shutter RamanOn_IndivFirstAMMS
masked_shutter testPhase
masked_shutter RamanOff


#Triggers
trigger ddsCoolingDetectTrigger = 3
trigger ddsRamanSBApply = 0
trigger ddsRamanApply_CounterProp = 0
trigger ddsRamanApply_CoProp = 0
trigger ddsRamanApply_CounterProp_Parity = 0
trigger ddsRamanMSApply = 0
trigger Shuttle_Apply = 0
trigger memsApply
trigger ddsRamanApply_CounterCarrier
trigger ddsRamanFreqSet

# DDS frequencies
parameter<AD9912_FRQ> CoolingFreq = 180 MHz
parameter<AD9912_FRQ> CoolingFreq2 = 185 MHz
parameter<AD9912_FRQ> DetectFreq = 195 MHz
parameter<AD9912_FRQ> PumpingFreq = 195 MHz
parameter <AD9912_FRQ> RamanFreq_CoProp = 200 MHz
parameter <AD9912_FRQ> RamanFreq_CounterProp = 250 MHz
parameter <AD9912_FRQ> RamanCarrierFreq_CounterProp = 250 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_ComZ = 203 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Com = 213 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Tilt = 213 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Tilt2 = 213 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Tilt3 = 213 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Tilt4 = 213 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Tilt5 = 213 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_Tilt6 = 213 MHz
parameter <AD9912_FRQ> RamanBlueSBFreq_Com = 246 MHz
parameter <AD9912_FRQ> RamanBlueSBFreq_Tilt = 246 MHz
parameter <AD9912_FRQ> RamanRedSBFreq_MS = 0 MHz
parameter <AD9912_FRQ> RamanBlueSBFreq_MS = 0 MHz
parameter <AD9912_FRQ> RamanFreq_Indiv_Shutter1 = 185 MHz
parameter <AD9912_FRQ> RamanFreq_Indiv_Shutter2 = 185 MHz
parameter <AD9912_FRQ> CalibrateFre = 200 MHz
parameter<AD9912_FRQ> MicrowaveSSBFreq = 42 MHz



#DDS Phases
parameter<AD9912_PHASE> RamanInitPhase = 0
parameter<AD9912_PHASE> RamanInitPhase180 = 0
parameter<AD9912_PHASE> RamanAnalyzePhase = 0
parameter<AD9912_PHASE> RamanParityPhase = 0
parameter<AD9912_PHASE> xPhase0 = 0
parameter<AD9912_PHASE> xPhase180 = 0

parameter<AD9912_FRQ> DetunOff1 = 0 kHz
parameter<AD9912_FRQ> DetunOff2 = 0 kHz

var indivPhase = 0
parameter<AD9912_PHASE> phaseShift = 180

var SBCoolingFreq
var DetunOffset = 0 kHz


#DDS Amplitudes
parameter CoolingAmp = 0
parameter CoolingAmp2 = 0
parameter PumpAmp = 0
parameter DetectAmp = 0
parameter RamanSBAmp_Blue = 0
parameter RamanSBAmp_Red = 0
parameter RamanMSAmp_Red = 0
parameter RamanMSAmp_Blue = 0
parameter RamanCarrierAmp = 0
parameter RamanIndiv1ShutterAmp = 0
parameter CalibrateAmp = 1000
parameter RamanCoAmp = 0

# Cool/Pump/Detect Times
parameter CoolingTime = 1 ms
parameter CoolingTimeRamanOnLockOff
parameter CoolingTime2 = 1 ms
parameter PumpTime = 0 ms
parameter PumpTimeSB = 0 ms
parameter DetectTime = 1 ms

#Microwave Times
parameter MicrowavePiTime = 0 us
parameter MicrowaveHalfPiTime = 0 us
parameter MicrowaveParityTime = 0 us

#Co-Propagating Raman Times
parameter RamanTime_CoProp = 0 ms
parameter RamanTime_CoProp2 = 0 ms
parameter RamanHalfPiTime_CoProp = 0 ms
parameter RamanHalfPiTime_CoProp2 = 0 ms
parameter RamanPiTime_CoProp = 0 us
parameter Raman2PiTime_CoProp = 0 us
parameter RamanAnalyzeTime_CoProp = 0 ms
parameter RamanParityTime_Co =0 us

#Counter-Propagating Raman Times
parameter RamanTime_CounterProp = 0 ms
parameter RamanHalfPiTime_CounterProp = 0 us
parameter RamanPiTime_CounterProp = 0 us
parameter Raman2PiTime_CounterProp = 0 us
parameter RamanAnalyzeTime_CounterProp = 0 ms
parameter RamanParityTime_CounterProp = 0 ms
parameter RamanParityTime_CounterProp4 = 0 ms
parameter IndivDelay = 10 us

#Sideband Cooling Times
parameter RamanRedSBPiTime_Tilt = 0 us
parameter RamanRedSBPiTime_Com = 0 us
parameter RSB_Step_1 = 0 us
parameter RSB_Step_2 = 0 us
parameter sbCoolDelayTime = 0 us
parameter sbCoolDelayTime2 = 0 us
parameter SBCool2 = 0
parameter SBCool3 = 0
parameter SBCool4 = 0
parameter SBCool5 = 0
parameter SBCool6 = 0
parameter SBCool7 = 0
parameter SBCoolCom = 0


#Molmer-Sorensen Times
parameter SingleQubitMSTime = 0 us
parameter SingleQubitMSTime_Half = 0 us
parameter TwoQubitMSTime = 0 us
parameter SBDelta = 0
parameter dDelta = 0
parameter SumDelta = 0
parameter phaseProtect = 0
parameter WalshGate = 0
parameter FMGate = 0
parameter AMGate = 0
parameter numMSGates = 1
parameter MSWalshTime = 0
parameter RamanStarkShiftDiff = 0 MHz
parameter MSFMTime = 0
parameter FMgateTimeOffset = 0 us

var FMgateTime1 = 0 us
var FMgateTime2 = 0 us





#Wait Times
parameter WaitTime = 0 ms
parameter QubitWaitTime = 0 ms
parameter QubitWaitTime2 = 0 ms
parameter updateDelay = 0 ms

#Shuttle Times
var Shuttle_Write_Delay = 20 us
var Shuttle_Post_Delay = 150 us
var Shuttling_Reaction_Time = 50 us
var Shuttle_Wait_Trigger = 0x1000000 
var Line_Trigger = 0x0000002

#Mems
parameter memsVoltages
parameter MEMSdelay
parameter n
parameter nX1
parameter nY1
parameter nX2
parameter nY2
parameter n2
parameter memsCalibrate
parameter memsVoltagesSet
parameter noffset

# control parameters
parameter experiments  = 100
parameter sbCoolIteration = 0
parameter doBB1 = 0
parameter echo = 0
parameter prepBright = 0
parameter fiberDetect = 0
parameter dummy
parameter adjustX = 0
parameter shuttleToCooling = 0
parameter shuttleToGate = 0
parameter circuit = 0
parameter ReverseCNOT = 0

# SK1 parameters
parameter<AD9912_PHASE> xPhi
parameter<AD9912_PHASE> xPhi1
parameter<AD9912_PHASE> xPhi2
parameter<AD9912_PHASE> yPhi
parameter<AD9912_PHASE> yPhi1
parameter<AD9912_PHASE> yPhi2


parameter<AD9912_PHASE> xSKHalfpi = 0
parameter<AD9912_PHASE> xSKMinusHalfpi = 0
#parameter<AD9912_PHASE> xSKHalfpiMinus = 0
#parameter<AD9912_PHASE> xSKMinusHalfpi = 0

#counters
counter CheckIonCounters = 0
counter FiberPMTCounter = 0

# internal variables
var SBCoolingLoopsLeft=0
var RedSidebandPiTime_1 = 40 us
var RedSidebandPiTime_2 = 40 us
var currentexperiment = 0
var currentMSGate = 0
var currentCircuit = 0
var coolPhotons = 0


#shuttling codes
var CoolingToGate = 0x0000000000000000
var GateToCooling = 0x0000000200000000

# exitcodes
exitcode IonLostExitcode = 0xfffe000000000001
exitcode endLabel = 0xfffe000000000000'''

basic_ops = '''#-------------------------------------------------------------------------------------------------------------------------------------------------
#Cooling, Pump, Detect
#-------------------------------------------------------------------------------------------------------------------------------------------------
def cool():
    #Doppler Cooling
    #set_dds( channel=DDSCooling, freq = CoolingFreq, amp= CoolingAmp )  
    set_dds( channel=DDSRaman_CounterProp, freq= CalibrateFre, amp = CalibrateAmp )
    set_trigger( ddsCoolingDetectTrigger )
    update()
    set_shutter( CoolingOnRamanOnLockOff )
    update( CoolingTimeRamanOnLockOff )
    set_shutter( CoolingOn )
    set_counter( CheckIonCounters )
    update( CoolingTime )

    #Do more Doppler Cooling closer to resonance
    #set_dds( channel=DDSCooling, freq = CoolingFreq2, amp= CoolingAmp2 )
    #set_trigger( ddsCoolingDetectTrigger )
    #update()
    #set_shutter( CoolingOn )
    #update( CoolingTime2 )

    set_shutter(CoolingOff_RamanOffLockOff)
    clear_counter()
    update()
    coolPhotons = load_count( PMTChannel )
    update()

def pump():
    #set_dds( channel=DDSCooling, freq = PumpingFreq, amp= PumpAmp )
    #set_trigger( ddsCoolingDetectTrigger )
    #update()
    set_shutter( PumpingOn )
    update( PumpTime )
    set_inv_shutter( PumpingOn )
    update()

def pumpSB():
    #set_dds( channel=DDSCooling, freq = PumpingFreq, amp= PumpAmp )
    #set_trigger( ddsCoolingDetectTrigger )
    #update()
    set_shutter( PumpingOn )
    update( PumpTimeSB )
    set_inv_shutter( PumpingOn )
    update()

def detectFiber():
    #set_dds( channel=DDSDetect, freq=DetectFreq, amp=DetectAmp )
    #set_trigger( ddsCoolingDetectTrigger )
    set_shutter( DetectOn )
    set_counter( FiberPMTCounter )
    update( DetectTime )
    set_inv_shutter( DetectOn )
    clear_counter()
    update()
'''

raman_ops = '''parameter<AD9912_PHASE> phaseIndv1SQG = 0
parameter<AD9912_PHASE> phaseIndv2SQG = 0
parameter<AD9912_PHASE> Indiv1ZPhase = 0
parameter<AD9912_PHASE> Indiv2ZPhase = 0
parameter<AD9912_PHASE> ZPhase = 0
parameter<AD9912_PHASE> phase0 = 0
parameter<AD9912_PHASE> phase90 = 90
parameter<AD9912_PHASE> phase180 = 180
parameter<AD9912_PHASE> phase270 = 270

masked_shutter RamanOn_Indv1
masked_shutter RamanOn_Indv2
masked_shutter RamanOn_Both

trigger ddsRamanApply_Indv1
trigger ddsRamanApply_Indv2
trigger ddsRamanApply_Both
    
def counterPropOnIndv1():
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG )
    set_dds(channel=DDSRaman_CounterProp_Parity, phase=RamanParityPhase)
    set_trigger( ddsRamanApply_Indv1 )
    update()
    set_shutter(RamanOn_Indv1)
    update(IndivDelay)
    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    set_inv_shutter( RamanOn_Indv1 )
    update()

def counterPropOnIndv2():
    set_dds(channel=DDSRaman_Indiv2, phase=phaseIndv2SQG )
    set_dds(channel=DDSRaman_CounterProp_Parity, phase=RamanParityPhase)
    set_trigger( ddsRamanApply_Indv2 )
    update()
    set_shutter(RamanOn_Indv2)
    update(IndivDelay)
    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    set_inv_shutter( RamanOn_Indv2 )
    update()


    
def counterPropOnBoth():
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG )
    set_dds(channel=DDSRaman_Indiv2, phase=phaseIndv2SQG )
    set_dds(channel=DDSRaman_CounterProp_Parity, phase=RamanParityPhase)
    set_trigger( ddsRamanApply_Both )
    update()
    set_shutter(RamanOn_Both)
    update(IndivDelay)
    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    set_inv_shutter( RamanOn_Both)
    update()

var skPhase = 0
parameter phaseUpdateDelay = 1 us

def counterPropOnIndv1SKHalfPi():
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG )
    set_trigger( ddsRamanApply_Indv1 )
    update()
    set_shutter(RamanOn_Indv1)
    update(IndivDelay)
    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    
    skPhase = phaseIndv1SQG+xSKHalfpi
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    set_trigger( ddsRamanApply_Indv1 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )
         
    skPhase = phaseIndv1SQG-xSKHalfpi
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    set_trigger( ddsRamanApply_Indv1 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )
    set_inv_shutter(RamanOn_Indv1)
    update()

def counterPropOnIndv1RCinSKHalfPi():
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG )
    set_trigger( ddsRamanApply_Indv1 )
    update()
    set_shutter(RamanOn_Indv1)
    update(IndivDelay)
    set_shutter(RamanOn_CounterCarrier)
    update(CORPSETime1, pulse_mode = True, wait_dds = True )
    
    skPhase = phaseIndv1SQG+phase180
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    set_trigger( ddsRamanApply_Indv1 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(CORPSETime2, pulse_mode = True, wait_dds = True )
         
    set_dds( channel=DDSRaman_Indiv1, phase=phaseIndv1SQG)
    set_trigger( ddsRamanApply_Indv1 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(CORPSETime3, pulse_mode = True, wait_dds = True )

    skPhase = phaseIndv1SQG+xSKHalfpi
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    set_trigger( ddsRamanApply_Indv1 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )
         
    skPhase = phaseIndv1SQG-xSKHalfpi
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    set_trigger( ddsRamanApply_Indv1 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True)
    set_inv_shutter( RamanOn_Indv1 )
    update()   

    

def counterPropOnIndv2SKHalfPi():
    
    set_dds(channel=DDSRaman_Indiv2, phase=phaseIndv2SQG )
    set_trigger( ddsRamanApply_Indv2 )
    update()
    set_shutter(RamanOn_Indv2)
    update(IndivDelay)

    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    
    skPhase = phaseIndv2SQG+xSKHalfpi
    set_dds( channel=DDSRaman_Indiv2, phase=skPhase)
    set_trigger( ddsRamanApply_Indv2)
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )

    #skPhase = RamanParityPhase+xSKHalfpiMinus
    skPhase = phaseIndv2SQG-xSKHalfpi
    set_dds( channel=DDSRaman_Indiv2, phase=skPhase)
    set_trigger( ddsRamanApply_Indv2 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True)
    set_inv_shutter( RamanOn_Indv2)
    update()   

def counterPropOnIndv2SKMinusHalfPi():
    skPhase = phaseIndv2SQG+phase180
    set_dds(channel=DDSRaman_Indiv2, phase=skPhase )
    #set_dds( channel=DDSRaman_CounterProp_Parity, phase=RamanParityPhase)
    set_trigger( ddsRamanApply_Indv2 )
    update()
    set_shutter(RamanOn_Indv2)
    update(IndivDelay)
    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    
    
    skPhase = phaseIndv2SQG+xSKMinusHalfpi
    set_dds( channel=DDSRaman_Indiv2, phase=skPhase)
    set_trigger( ddsRamanApply_Indv2)
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )

    #skPhase = RamanParityPhase+xSKHalfpiMinus
    skPhase = phaseIndv2SQG-xSKMinusHalfpi
    set_dds( channel=DDSRaman_Indiv2, phase=skPhase)
    set_trigger( ddsRamanApply_Indv2 )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True)
   
    set_inv_shutter( RamanOn_Indv2)
    update()   

def counterPropOnBothSKHalfPi():
    
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG)
    set_dds(channel=DDSRaman_Indiv2, phase=phaseIndv2SQG)
    #set_dds(channel=DDSRaman_CounterProp_Parity, phase=RamanParityPhase)
    set_trigger(ddsRamanApply_Both)
    update()
    set_shutter(RamanOn_Both)
    update(IndivDelay)

    set_shutter(RamanOn_CounterCarrier)
    update(RamanHalfPiTime_CounterProp, pulse_mode = True, wait_dds = True )
    
    skPhase = phaseIndv2SQG+xSKHalfpi
    set_dds( channel=DDSRaman_Indiv2, phase=skPhase)
    

    skPhase = phaseIndv1SQG+xSKHalfpi
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    

    set_trigger( ddsRamanApply_Both )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )

    skPhase = phaseIndv2SQG-xSKHalfpi
    set_dds( channel=DDSRaman_Indiv2, phase=skPhase)
    #write_result(debugchannel, skPhase)
    skPhase = phaseIndv1SQG-xSKHalfpi
    set_dds( channel=DDSRaman_Indiv1, phase=skPhase)
    #write_result(debugchannel2, skPhase)

    set_trigger( ddsRamanApply_Both )
    update(phaseUpdateDelay)
    set_shutter(RamanOn_CounterProp_Parity)
    update(RamanParityTime_CounterProp4, pulse_mode = True, wait_dds = True )
    
    set_inv_shutter(RamanOn_Both)
    update()
'''
sbcool = '''#-------------------------------------------------------------------------------------------------------------------------------------------------
#Sideband Cooling Functions
#-------------------------------------------------------------------------------------------------------------------------------------------------
def sidebandCoolSequence():
    set_dds( channel=DDSRaman_CounterProp_SBCool, freq = SBCoolingFreq, amp = RamanCarrierAmp)
    set_trigger( ddsRamanSBApply )
    update()
    #counterPropRamanSwitchDelay()
    SBCoolingLoopsLeft = sbCoolIteration+1
    RedSidebandPiTime_1 = RamanRedSBPiTime_Com
    while SBCoolingLoopsLeft >= 0:
        RedSidebandPiTime_1 += RSB_Step_1
        set_shutter( RamanSBOn )
        update( RedSidebandPiTime_1, pulse_mode=True, wait_dds=True )
        update()
        pumpSB()
        SBCoolingLoopsLeft -= 1
    update()

def sidebandCoolComZ():
    set_dds( channel=DDSRaman_CounterProp_SBCool, freq = RamanRedSBFreq_ComZ, amp = RamanCarrierAmp)
    set_trigger( ddsRamanSBApply )
    update()
    #counterPropRamanSwitchDelay()
    SBCoolingLoopsLeft = sbCoolIteration+1
    RedSidebandPiTime_2 = RamanRedSBPiTime_Com
    while SBCoolingLoopsLeft >= 0:
        RedSidebandPiTime_2 += RSB_Step_2
        set_shutter( RamanSBOn )
        update( RedSidebandPiTime_2, pulse_mode=True, wait_dds=True )
        update()
        pumpSB()
        SBCoolingLoopsLeft -= 1
    update()

def sidebandCool():
    counterPropRamanTurnOnIndiv()
    if SBCool2>0:
        SBCoolingFreq = RamanRedSBFreq_Tilt2
        sidebandCoolSequence()
    if SBCool3>0:
        SBCoolingFreq = RamanRedSBFreq_Tilt3
        sidebandCoolSequence()
    if SBCool4>0:
        SBCoolingFreq = RamanRedSBFreq_Tilt4
        sidebandCoolSequence()
    if SBCool5>0:
        SBCoolingFreq = RamanRedSBFreq_Tilt5
        sidebandCoolSequence()
    if SBCool6>0:
        SBCoolingFreq = RamanRedSBFreq_Tilt6
        sidebandCoolSequence()
    

    if SBCoolCom == 0:
        SBCoolingFreq = RamanRedSBFreq_Com
        sidebandCoolSequence()
    
    if SBCool7>0:
        SBCoolingFreq = RamanRedSBFreq_Tilt
        sidebandCoolSequence()
    
    #sidebandCoolComZ()
    
    
    if SBCoolCom>0:
        SBCoolingFreq = RamanRedSBFreq_Com
        sidebandCoolSequence()
    #sidebandCoolComZ()
    
    counterPropRamanTurnOffIndiv()

def sbCoolDelay():
    set_shutter(DelayOn)
    update(sbCoolDelayTime)
    set_inv_shutter(DelayOn)
    update()
'''

msfunctions = '''#-------------------------------------------------------------------------------------------------------------------------------------------------
#Molmer-Sorensen Functions
#-------------------------------------------------------------------------------------------------------------------------------------------------
var segments = 0
parameter<AD9912_FRQ> redFreq
parameter<AD9912_FRQ> blueFreq
parameter<AD9912_FRQ> redFreqSet
parameter<AD9912_FRQ> blueFreqSet

parameter FMsegments =20
address RamStartAddress = 0
parameter WaitForRam = 5 us

parameter<AD9912_FRQ> offset10p
parameter<AD9912_FRQ> offset10n
parameter<AD9912_PHASE> MSZPhaseCompensate = 0

def FMGateMSRAM():
    set_ram_address( RamStartAddress )
    update( WaitForRam)
    
    set_trigger( ddsRamanSBApply )
    update()
    set_shutter( RamanOn_Both )
    redFreq = read_ram()
    blueFreq = read_ram()
    update( IndivDelay )
    #write_result(debugChannel,blueFreq)
    redFreqSet =  redFreq + offset10p
    redFreqSet =  redFreqSet - offset10n
        
    blueFreqSet = blueFreq - offset10p
    blueFreqSet = blueFreqSet + offset10n    
    set_dds( channel=DDSRamanSB1, freq =redFreqSet)
    set_dds( channel=DDSRamanSB2, freq =blueFreqSet)
    set_trigger( ddsRamanMSApply)
    update()
    set_shutter( RamanMSOn )
    redFreq = read_ram()
    blueFreq = read_ram()
    update(MSFMTime)
    segments = 1    
    while segments < FMsegments:
        
        redFreqSet =  redFreq + offset10p
        redFreqSet =  redFreqSet - offset10n
        
        blueFreqSet = blueFreq - offset10p
        blueFreqSet = blueFreqSet + offset10n

        set_dds( channel=DDSRamanSB1, freq =redFreqSet)
        set_dds( channel=DDSRamanSB2, freq =blueFreqSet)
        set_trigger( ddsRamanMSApply)
        update()
        redFreq = read_ram()
        blueFreq = read_ram()
        update(MSFMTime)
        segments += 1

    set_inv_shutter(RamanMSOn)
    update()
    set_inv_shutter(RamanOn_Both)
    update()
    Indiv2ZPhase = Indiv2ZPhase+MSZPhaseCompensate
    Indiv1ZPhase = Indiv1ZPhase+MSZPhaseCompensate
'''

gates = '''#-------------------------------------------------------------------------------------------------------------------------------------------------
#gates 
#-------------------------------------------------------------------------------------------------------------------------------------------------
def x_halfpi_1():
    phaseIndv1SQG = Indiv1ZPhase+phase0
    counterPropOnIndv1()
def y_halfpi_1():
    phaseIndv1SQG = Indiv1ZPhase+phase90
    counterPropOnIndv1()
def z_halfpi_1():
    Indiv1ZPhase = Indiv1ZPhase+phase270
def z_onepi_1():
    Indiv1ZPhase = Indiv1ZPhase+phase180
def z_neghalfpi_1():
    Indiv1ZPhase = Indiv1ZPhase+phase90

def x_halfpi_2():
    phaseIndv2SQG = Indiv2ZPhase+phase0
    counterPropOnIndv2()
def y_halfpi_2():
    phaseIndv2SQG = Indiv2ZPhase+phase90
    counterPropOnIndv2()
def z_halfpi_2():
    Indiv2ZPhase = Indiv2ZPhase+phase270
def z_onepi_2():
    Indiv2ZPhase = Indiv2ZPhase+phase180
def z_neghalfpi_2():
    Indiv2ZPhase = Indiv2ZPhase+phase90

def xx_pos():
    phaseIndv1SQG = Indiv1ZPhase+phase0
    phaseIndv2SQG = Indiv2ZPhase+phase180
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG )
    set_dds(channel=DDSRaman_Indiv2, phase=phaseIndv2SQG )
    set_trigger(ddsRamanApply_Both)
    update(phaseUpdateDelay)
    FMGateMSRAM()
def xx_neg():
    phaseIndv1SQG = Indiv1ZPhase+phase0
    phaseIndv2SQG = Indiv2ZPhase+phase0
    set_dds(channel=DDSRaman_Indiv1, phase=phaseIndv1SQG )
    set_dds(channel=DDSRaman_Indiv2, phase=phaseIndv2SQG )
    set_trigger(ddsRamanApply_Both)
    update(phaseUpdateDelay)
    FMGateMSRAM()
'''

mems = '''#-------------------------------------------------------------------------------------------------------------------------------------------------
#Mems
#-------------------------------------------------------------------------------------------------------------------------------------------------
def setMemsVoltage():
    update( MEMSdelay )
    serial_write( memsCommand, memsVoltages )
    set_trigger( memsApply )
    update( MEMSdelay )
'''

exp_seq_prepare = '''#-------------------------------------------------------------------------------------------------------------------------------------------------
#Experimental Sequence
#-------------------------------------------------------------------------------------------------------------------------------------------------
set_shutter( InitializationShutter )
update()
while not pipe_empty():
    update()
    apply_next_scan_point()
    
    set_dds( channel=DDSRaman_CounterProp_Parity, freq=RamanCarrierFreq_CounterProp, phase=RamanParityPhase, amp = RamanCarrierAmp)
    set_dds( channel=DDSRamanSB1, freq = RamanRedSBFreq_MS, amp=RamanMSAmp_Red, phase=RamanInitPhase)
    set_dds( channel=DDSRamanSB2, freq = RamanBlueSBFreq_MS, amp=RamanMSAmp_Blue, phase=RamanInitPhase)
    set_dds( channel=DDSRaman_Indiv1, freq = RamanFreq_Indiv_Shutter1, phase=RamanInitPhase)
    set_dds( channel=DDSRaman_Indiv2, freq = RamanFreq_Indiv_Shutter2, phase=RamanInitPhase)
    set_trigger(ddsRamanFreqSet)
    update()
    
    currentexperiment = 0
    DetunOffset = DetunOffset - DetunOff1
    while currentexperiment < experiments:
        DetunOffset = DetunOffset + DetunOff2
        
        if memsCalibrate>0:
            setMemsVoltage()

        if CoolingTime>0:
            cool()

        if PumpTime>0:
            pump()

        if sbCoolIteration>0:
            sidebandCool()
            pump()

        if sbCoolDelayTime >0:
            sbCoolDelay()
        
        if prepBright > 0:
            counterPropRaman1pi()
'''

exp_seq_end = '''        if DetectTime>0:
            detectFiber()
                
        currentexperiment += 1
exit( endLabel )
'''