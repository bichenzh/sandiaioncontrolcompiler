import cirq
import cirq.ion as ci
import artiq_compiler as ac
import numpy as np
import pandas as pd
import gate as gt
import code_block as cb

qubit_num = 2
qubit_list = cirq.LineQubit.range(qubit_num)

rep_times = 1
theta = np.pi/9


### make your ion trap device with desired gate times and qubits
us = 1000*cirq.Duration(nanos=1)
ion_device = ci.IonDevice(measurement_duration=100*us,
                        twoq_gates_duration=200*us,
                        oneq_gates_duration=10*us,
                        qubits=qubit_list)

### some clifford gate
circuit = cirq.Circuit()
for i in range(qubit_num):
    circuit.append([cirq.H(qubit_list[i])])
for _ in range(rep_times):
    circuit.append([(cirq.ry(0.5*np.pi))(qubit_list[0])])
    circuit.append([cirq.ms(np.pi/4)(qubit_list[0],qubit_list[1])])
    circuit.append([(cirq.rx(-0.5*np.pi))(qubit_list[0])])
    circuit.append([(cirq.rx(-0.5*np.pi))(qubit_list[1])])
    circuit.append([(cirq.ry(-0.5*np.pi))(qubit_list[0])])
    # circuit.append([cirq.CNOT(qubit_list[0],qubit_list[1])])

    circuit.append([cirq.Z(qubit_list[1])**(theta/np.pi)])

    # circuit.append([cirq.CNOT(qubit_list[0],qubit_list[1])])

    # circuit.append([(cirq.ry(0.5*np.pi))(qubit_list[0])])
    # circuit.append([cirq.ms(np.pi/4)(qubit_list[0],qubit_list[1])])
    # circuit.append([(cirq.rx(-0.5*np.pi))(qubit_list[0])])
    # circuit.append([(cirq.rx(-0.5*np.pi))(qubit_list[1])])
    # circuit.append([(cirq.ry(-0.5*np.pi))(qubit_list[0])])

    circuit.append([(cirq.ry(0.5*np.pi))(qubit_list[0])])
    circuit.append([(cirq.rx(0.5*np.pi))(qubit_list[0])])
    circuit.append([(cirq.rx(0.5*np.pi))(qubit_list[1])])
    circuit.append([cirq.ms(-np.pi/4)(qubit_list[0],qubit_list[1])])
    circuit.append([(cirq.ry(-0.5*np.pi))(qubit_list[0])])

for i in range(qubit_num):
    circuit.append([cirq.H(qubit_list[i])])

print("Clifford Circuit:")
print(circuit)
### convert the clifford circuit into circuit with ion trap native gates
ion_circuit = ion_device.decompose_circuit(circuit)
print("Iontrap Circuit:")
print(ion_circuit)

ac1 = ac.ArtiqCompiler(ion_circuit)

print('Gate sequence (operations in the same row can be done simultaneously):\n',pd.DataFrame(ac1.ops_list),'\n')
print('Print the gates one by one:')
for moment in ac1.ops_list:
    for op in moment:
        print('Axis:', op.axis,'\tQubits:',op.channels(),'\tRotation:%+.2f\u03C0'%(op.exp),'\tPhase:%+.2f\u03C0'%(op.phase_exponent))

mems_dict = {0:'1',1:'2'}
# print(mems_dict[0])
# print(cirq.PhasedXPowGate(phase_exponent = 0.5,exponent=0.5)._pauli_expansion_())
circ_code_block='''        if circuit >0:
                               
            Indiv1ZPhase = 0
            Indiv2ZPhase = 0
            currentCircuit = 0
            while currentCircuit < circuit:
'''

def preZphase(op, circ_code_block, indv, direct):
    if op.isPhased:
        if direct*op.phase_exponent == 0.5:
            circ_code_block += ' '*4*4+'z_neghalfpi_'+indv+'()'+'\n'
        elif direct*op.phase_exponent == -0.5:
            circ_code_block += ' '*4*4+'z_halfpi_'+indv+'()'+'\n'
        elif op.phase_exponent == 1 or op.phase_exponent == -1:
            circ_code_block += ' '*4*4+'z_onepi_'+indv+'()'+'\n'
        else:
            raise ValueError('The rotation angle is not supported')
    return circ_code_block

def postZphase(op, circ_code_block, indv, direct):
    if op.isPhased:
        if direct*op.phase_exponent == 0.5:
            circ_code_block += ' '*4*4+'z_halfpi_'+indv+'()'+'\n'
        elif direct*op.phase_exponent == -0.5:
            circ_code_block += ' '*4*4+'z_neghalfpi_'+indv+'()'+'\n'
        elif op.phase_exponent == 1 or op.phase_exponent == -1:
            circ_code_block += ' '*4*4+'z_onepi_'+indv+'()'+'\n'
        else:
            raise ValueError('The rotation angle is not supported')
    return circ_code_block

def equator_gate(op, circ_code_block):
    axis = (op.axis).lower()
    exp = op.exp
    print(op)
    if exp*2 - int(exp*2) != 0:
        circ_code_block += 'SPECIAL ROTATION ANGLE ON EQUATOR!!!!\n'
        return circ_code_block
    indv = mems_dict[op.channels()[0]]
    direct = -1 if op.exp<0 else 1
    func_name = axis+'_halfpi_'+indv+'()'
    circ_code_block = preZphase(op, circ_code_block, indv, direct)
    for i in range(int(exp*2)):
        circ_code_block += ' '*4*4+func_name+'\n'
    circ_code_block = postZphase(op, circ_code_block, indv, direct)
    return circ_code_block

for moment in ac1.ops_list:
    for op in moment:
        # leave mems voltage control for now
        if op.axis == 'XX':
            func_name = 'xx_pos()' if op.exp>0 else 'xx_neg()'
            circ_code_block += ' '*4*4+func_name+'\n'
        elif op.axis == 'X' or op.axis == 'Y':
            circ_code_block = equator_gate(op,circ_code_block)
        elif op.axis == 'Z':
            indv = mems_dict[op.channels()[0]]
            if op.exp == 0.5:
                circ_code_block += ' '*4*4+'z_halfpi_'+indv+'()\n'
            elif op.exp == -0.5:
                circ_code_block += ' '*4*4+'z_neghalfpi_'+indv+'()\n'
            elif op.exp == 1 or op.exp == -1:
                circ_code_block += ' '*4*4+'z_onepi_'+indv+'()\n'
            else:
                print('Do you want to scan Z phase in your circuit?')
                circ_code_block += ' '*4*4+'Indiv'+indv+'ZPhase = Indiv'+indv+'ZPhase+ZPhase\n'

print(cb.exp_seq_prepare+circ_code_block+cb.exp_seq_end)
