import numpy as np
import cirq as ci
import gate as gt

class ArtiqCompiler(object):
    def __init__(self, ion_circuit: ci.circuits.circuit.Circuit):
        self.ion_circuit = ion_circuit
        self.sq_gate_time = 10
        self.ms_gate_time = 200
        self.ops_list = self.toObjList()

    def toObjList(self):
        z_names = ['Z','Rz','Z*','S','T']
        x_names = ['X','Rx','X*']
        y_names = ['Y','Ry','Y*']
        xx_names = ['MS','XX']
        ops_list = []
        for mt in self.ion_circuit._moments:
            ops_mt = []
            for op in mt:
                name = str(op.gate)[0:2]
                channels = []
                for channel in op.qubits:
                    channels.append(int(str(channel)))
                try:
                    exponent = op.gate.exponent
                except:
                    exponent = 0.0
                    name = 'DT'
                if name in x_names:
                    ops_mt.append(gt.SQGate('X', channels[0], self.sq_gate_time, exponent))
                elif name in y_names:
                    ops_mt.append(gt.SQGate('Y', channels[0], self.sq_gate_time, exponent))
                elif name in z_names:
                    ops_mt.append(gt.SQGate('Z', channels[0], self.sq_gate_time, exponent))
                elif name in xx_names:
                    ops_mt.append(gt.MSGate('XX', channels[0], channels[1], self.ms_gate_time, exponent/2))
                elif name == 'Ph': # phased x gate
                    axis = str(op.gate)[2]
                    #todo: add phasedX
                    phase_exponent = op.gate.phase_exponent
                    ops_mt.append(gt.SQGate(axis, channels[0], self.sq_gate_time, exponent, phase_exponent))
                elif name == 'DT':
                    # todo: add measurement gates
                    ops_mt.append(None)
                else:
                    raise TypeError('unkown operation')
            ops_list.append(ops_mt)
        return ops_list

