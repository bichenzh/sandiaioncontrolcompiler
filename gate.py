import numpy as np

class Gate(object):

    def __init__(self):
        # rotation angle of the gate
        self.exp = 0
        self.full_gate_time = 0
        self.duration = 0

    # set rotation and set duration
    def set_exp(self, exp):
        self.exp = exp
        self.set_duration()

    def set_duration(self):
        self.duration = np.abs(self.exp) * self.full_gate_time


    def __repr__(self):
        return "<UndefinedGate>"
    def __str__(self):
        return "<UndefinedGate>"

class SQGate(Gate):
    def __init__(self, axis, qreg, full_gate_time, exponent, phase_exponent=0):
        self.axis = axis    # x, y, or z
        self.qreg = qreg
        self.full_gate_time = full_gate_time
        self.exp = exponent
        self.set_duration()
        self.isReversed = exponent < 0
        self.isPhased = phase_exponent!=0
        self.phase_exponent = phase_exponent

    def channels(self):
        return np.array([self.qreg])

    def __repr__(self):
        if not self.isPhased:
            return "<R%s%s(%.2f * \u03C0)>" % (self.axis, self.channels(), self.exp)
        else:
            return "<Ph%s%s(%.2f * \u03C0, %.2f * \u03C0)>" % (self.axis, self.channels(), self.exp, self.phase_exponent)

    def __str__(self):
        if not self.isPhased:
            return "<R%s%s(%.2f * \u03C0)>" % (self.axis, self.channels(), self.exp)
        else:
            return "<Ph%s%s(%.2f * \u03C0, %.2f * \u03C0)>" % (self.axis, self.channels(), self.exp, self.phase_exponent)

class MSGate(Gate):
    def __init__(self, axis, qreg1, qreg2, full_gate_time, exponent=0.25, phase_exponent=0):
        self.axis = axis
        self.exp = exponent
        self.qreg1 = qreg1
        self.qreg2 = qreg2
        self.full_gate_time = full_gate_time    # max entanglement time
        self.set_duration()
        self.phase_exponent = phase_exponent

    def set_duration(self):
        self.duration = self.exp / 0.25 * self.full_gate_time

    def channels(self):
        return np.array([self.qreg1,self.qreg2])

    def __repr__(self):
        return "<MS%s(%.2f * \u03C0)>" % (self.channels(), self.exp)
    def __str__(self):
        return "<MS%s(%.2f * \u03C0)>" % (self.channels(), self.exp)
